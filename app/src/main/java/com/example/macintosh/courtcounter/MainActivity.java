package com.example.macintosh.courtcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private int teamAScore =0;
    private int teamBScore = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayForTeamA(teamAScore);
        displayForTeamB(teamBScore);
    }

    public void freethrow(View view){
        switch(view.getId()){
            case(R.id.freeThrowForTeamA): teamAScore+=1;displayForTeamA(teamAScore);break;
            case(R.id.freeThrowForTeamB): teamBScore+=1; displayForTeamB(teamBScore);break;
        }
    }

    public void twoPoints(View view){
        switch(view.getId()){
            case(R.id.twoPointsForTeamA): teamAScore+=2;displayForTeamA(teamAScore);break;
            case(R.id.twoPointsForTeamB): teamBScore+=2; displayForTeamB(teamBScore);break;
        }
    }

    public void threePoints(View view){
        switch(view.getId()){
            case(R.id.threePointsForTeamA): teamAScore+=3;displayForTeamA(teamAScore);break;
            case(R.id.threePointsForTeamB): teamBScore+=3; displayForTeamB(teamBScore);break;
        }
    }

    public void resetAll(View view){
        teamBScore =0;
        teamAScore = 0;
        displayForTeamB(teamBScore);
        displayForTeamA(teamAScore);
    }

    private void displayForTeamB(int teamB_Score) {
        TextView scoreView =  findViewById(R.id.teamBScoreTextView);
        scoreView.setText(String.valueOf(teamB_Score));
    }

    /**
     * Displays the given teamAScore for Team A.
     */
    public void displayForTeamA(int teamA_Score) {
        TextView scoreView =  findViewById(R.id.teamAScoreTextView);
        scoreView.setText(String.valueOf(teamA_Score));
    }
}
